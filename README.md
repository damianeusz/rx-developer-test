Uruchomienie ćwiczenia

konsola:
php composer.phar install
phpunit -c app

app/console doctrine:schema:create
app/console doctrine:fixtures:load
/usr/bin/php -S localhost:8011 -t ./web

przeglądarka:
http://localhost:8011/app_dev.php/