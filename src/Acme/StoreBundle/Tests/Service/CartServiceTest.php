<?php


namespace Acme\StoreBundle\Service;


use Acme\StoreBundle\Entity\Cart;
use Acme\StoreBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\MockFileSessionStorage;

class CartServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var SessionInterface
     */
    private $session;
    private $sessionDir;

    public function setUp()
    {
        $this->sessionDir = sys_get_temp_dir().'/cart_test';
        $this->session = new Session(new MockFileSessionStorage($this->sessionDir));
    }

    protected function tearDown()
    {
        $this->sessionDir = null;
        array_map('unlink', glob($this->sessionDir.'/*.session'));
        if (is_dir($this->sessionDir)) {
            rmdir($this->sessionDir);
        }
    }

    /**
     * @test
     */
    public function should_load_empty_cart()
    {
        $cartService = new CartService($this->session);
        $this->assertEquals($cartService->loadCart(), new Cart());
    }

    /**
     * @test
     */
    public function should_add_product_to_cart()
    {
        $cartService = new CartService($this->session);
        $product = new Product('dd');
        $cartService->addProduct($product);

        $this->assertEquals($cartService->loadCart()->getProducts()->get(0), $product);
    }

    /**
     * @test
     */
    public function should_save_cart_in_session()
    {
        $cartService = new CartService($this->session);
        $product = new Product('dd');
        $cartService->addProduct($product);

        unset($cartService);

        $cartService = new CartService($this->session);
        $this->assertEquals($cartService->loadCart()->getProducts()->get(0), $product);
        $this->assertCount(1, $cartService->loadCart()->getProducts());
    }

    /**
     * @test
     */
    public function should_clear_stored_cart()
    {
        $cartService = new CartService($this->session);
        $product = new Product('dd');
        $cartService->addProduct($product);
        $cartService->clear();

        unset($cartService);

        $cartService = new CartService($this->session);
        $this->assertCount(0, $cartService->loadCart()->getProducts());
    }
}
 