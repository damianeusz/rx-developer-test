<?php

namespace Acme\StoreBundle\Controller;

use Acme\StoreBundle\Form\SelectProductType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Method("get")
     * @Route("/", name="index")
     *
     * @return Response
     */
    public function indexAction()
    {
        $form = $this->createForm(new SelectProductType());
        $cartService = $this->get('cart_service');

        return $this->render(
            'AcmeStoreBundle:Default:index.html.twig',
            array(
                'form' => $form->createView(),
                'cart' => $cartService->loadCart(),
            )
        );
    }

    /**
     * @Method("post")
     * @Route("/", name="submit")
     *
     * @return Response
     */
    public function submitAction(Request $request)
    {
        $form = $this->createForm(new SelectProductType());
        $form->handleRequest($request);
        $selectedProduct = $form->getData();

        $cartService = $this->get('cart_service');
        $cartService->addProduct($selectedProduct->getProduct());

        return $this->render(
            'AcmeStoreBundle:Default:index.html.twig',
            array(
                'form' => $form->createView(),
                'cart' => $cartService->loadCart(),
            )
        );
    }

    /**
     * @Method("get")
     * @Route("/empty-cart", name="empty")
     *
     * @return Response
     */
    public function emptyCartAction()
    {
        $this->get('cart_service')->clear();

        return $this->redirect($this->generateUrl('index'));
    }

}
