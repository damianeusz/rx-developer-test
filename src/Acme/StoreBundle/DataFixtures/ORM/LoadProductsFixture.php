<?php

namespace Acme\StoreBundle\DataFixtures\ORM;

use Acme\StoreBundle\Entity\Product;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadProductsFixture implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $productNames = array('Jabłka', 'Śliwki', 'Gruszki');

        foreach ($productNames as $productName) {
            $product = new Product();
            $product->setName($productName);

            $manager->persist($product);
        }

        $manager->flush();
    }
}