<?php

namespace Acme\StoreBundle\Logger;


use Acme\StoreBundle\Entity\Log;
use Acme\StoreBundle\Entity\ProductLog;
use Doctrine\Common\Persistence\ObjectManager;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;

class DoctrineHandler extends AbstractProcessingHandler
{

    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @param ObjectManager $manager
     * @param bool|int $level
     * @param bool $bubble
     */
    public function __construct(ObjectManager $manager, $level = Logger::DEBUG, $bubble = true){
        parent::__construct($level, $bubble);
        $this->manager = $manager;
    }

    /**
     * Writes the record down to the log of the implementing handler
     *
     * @param  array $record
     * @return void
     */
    protected function write(array $record)
    {
        $logitem = new Log();
        $logitem->setChannel($record['channel']);
        $logitem->setLevel($record['level']);
        $logitem->setMessage($record['formatted']);
        $logitem->setCreatedAt($record['datetime']);

        $this->manager->persist($logitem);
        $this->manager->flush();
    }
}