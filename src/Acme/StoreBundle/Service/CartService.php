<?php

namespace Acme\StoreBundle\Service;

use Acme\StoreBundle\Entity\Cart;
use Acme\StoreBundle\Entity\Product;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CartService
{
    const CART_SESSION_KEY = '_cart_key';

    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var Logger
     */
    private $logger;

    public function __construct(SessionInterface $session, Logger $logger = null)
    {
        $this->session = $session;
        $this->logger = $logger;
    }

    /**
     * @return Cart
     */
    public function loadCart()
    {
        return $this->session->get(self::CART_SESSION_KEY, new Cart());
    }

    /**
     * @param Product $product
     */
    public function addProduct(Product $product)
    {
        $cart = $this->loadCart();
        $cart->addProduct($product);

        $this->session->set(self::CART_SESSION_KEY, $cart);

        if ($this->logger) {
            $this->logger->info("Product '{$product->getName()}' selected to cart.");
        }
    }

    public function clear()
    {
        $this->session->remove(self::CART_SESSION_KEY);
    }
}