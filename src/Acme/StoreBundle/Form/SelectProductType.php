<?php

namespace Acme\StoreBundle\Form;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class SelectProductType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('product', 'entity', array(
                    'class' => 'AcmeStoreBundle:Product',
                    'property'  => 'name',
                    'multiple'  => false,
                    'expanded'  => true,
                    'label'     => 'Available products:'
                ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Acme\StoreBundle\Form\Data\SelectedProduct',
            ));
    }

    /**
     * @return string The name of this type
     */
    public function getName()
    {
        return 'select_product';
    }
}