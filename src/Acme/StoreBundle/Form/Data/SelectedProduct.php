<?php

namespace Acme\StoreBundle\Form\Data;


use Acme\StoreBundle\Entity\Product;

class SelectedProduct
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    public function getProductName()
    {
        return $this->product->getName();
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }
}